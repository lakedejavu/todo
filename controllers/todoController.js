var bodyParser = require('body-parser');
var mongoose = require('mongoose');

//connect db
mongoose.connect('mongodb://localhost:27017/todo',{useNewUrlParser: true });

//Schema
var todoSchema = new mongoose.Schema({
    item: String
});

//model 
var Todo = mongoose.model('Todo', todoSchema);
// var itemOne = Todo({item: 'buy drinks'}).save((err)=>{
//     if(err) throw err;
//     console.log('item saved');
// });


// var data = [
//     {item: 'get fruits'},{item: 'do laundry'},{item: 'study and code'}
// ];
var urlencodedParser = bodyParser.urlencoded({extended: false});

module.exports = (app)=>{

    
    app.get('/todo', (req,res)=>{
        //get data from mongodb and pass it to view
        Todo.find({}, (err,data)=>{
            if(err) throw err;
            res.render('todo', {todos:data});  
        });       
             
    });
    app.post('/todo', urlencodedParser, (req,res)=>{
        //get data from the view and add it to mongodb
        var newTodo = Todo(req.body).save((err,data)=>{
            if(err) throw err;
            res.json(data);
        })
       
    });
    app.delete('/todo/:item', (req,res)=>{
        //delete the requested item from mongodb
        Todo.find({item:req.params.item.replace(/\-/g, ' ')}).remove((err,data)=>{
            if(err) throw err;
            res.json(data);
        });
    //     data = data.filter((todo)=>{
    //         return todo.item.replace(/ /g, '-') !== req.params.item;
    //     });
    //    res.json(data); 
    });
};