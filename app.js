var express = require('express');

var todoController = require('./controllers/todoController');


var app = express();

//set up templates engine
app.set('view engine', 'ejs');

//static files
app.use(express.static('./public'));

//fire controllers
todoController(app);

//listen port
app.listen(3000);
console.log('You are listening port 3000' );
